# Site AIM

Test de la part de l'entreprise AIM pour déterminé mon niveau en tant que développeur web !

## Installation du projet

Pour cela il faut effectuer quelques commandes simple comme :

- Cloner le projet sur son ordinateur avec la commande `git clone https://gitlab.com/Godardx/site-aim.git`
- Lancer le projet en double cliquant sur `index.html` ou en lançant un Live Server (en ayant l'extension d'installer sur VS Code)

Sinon si vous disposez du dossier avec le site et que vous ne disposez pas de VS Code il vous suffira de lancer le projet en double cliquant sur `index.html`
